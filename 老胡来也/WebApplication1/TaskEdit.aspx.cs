﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var see = Session["Username"];

            if (see == null)
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                var id = Request.QueryString["id"];

                if (!string.IsNullOrEmpty(id))
                {
                    var sql = string.Format("select * from Task where id={0}", id);

                    var dt = DbHelper.GetData(sql);

                    if (dt.Rows.Count > 0)
                    {
                        var row = dt.Rows[0];

                        this.txtId.Text = id;
                        this.txtName.Text = row["TaskName"].ToString();
                        this.txtContent.Text = row["TaskContent"].ToString();
                        this.txtRemarks.Text = row["Remarks"].ToString();
                    }
                }
            }

            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var id = this.txtId.Text;
            var name = this.txtName.Text;
            var content = this.txtContent.Text;
            var remarks = this.txtRemarks.Text;

            if (string.IsNullOrEmpty(id))
            {
                //从Session获得当前登录用户的Id
                var userId = Session["userid"].ToString();

                //新增
                var sql = string.Format("insert into Task(TaskName,TaskContent,CreatedUserId,Remarks) values ('{0}','{1}',{2},'{3}')", name, content, userId, remarks);
                var inserted = DbHelper.Exe(sql);


            }
            else
            {
                //更新
                var sql = string.Format("update Task set TaskName='{0}',TaskContent='{1}',Remarks='{2}' where id={3}", name, content, remarks,id);
                var updated = DbHelper.Exe(sql);
            }


            Response.Redirect("Default.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}