﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th{
            text-align:center;
        }
        td{
            width:200px;
            text-align:center;
        }
        /*#left{
            float:left;
        }*/
        /*#right{
            float:right;
        }*/
    </style>
    <div>
        <%--<div id="left">
            <asp:Button runat="server" ID="btnAdd" Text="添加" OnClick="btnAdd_Click"/>
        </div>--%>
        <div id="right">
            <asp:Button runat="server" ID="Button1" Text="添加" OnClick="btnAdd_Click"/>
                <asp:TextBox runat="server" ID="txtKey" placeholder="请输入关键字"></asp:TextBox>

                <asp:Button runat="server" ID="btnSearch" Text="搜索" OnClick="btnSearch_Click"/>

            <asp:Button runat="server" ID="btnClear" Text="清除搜索关键字" OnClick="btnClear_Click"/>

        </div>        
    </div>
    
    
    
    <asp:GridView runat="server" AllowPaging="true" ID="GrdList" AutoGenerateColumns="false" OnRowCommand="GrdList_RowCommand" OnRowCancelingEdit="GrdList_RowCancelingEdit" OnPageIndexChanging="GrdList_PageIndexChanging"
        OnRowDeleting="GrdList_RowDeleting" OnRowEditing="GrdList_RowEditing" OnRowUpdating="GrdList_RowUpdating">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true"/>
            <asp:BoundField DataField="TaskName" HeaderText="任务名称"/>
            <asp:BoundField DataField="TaskContent" HeaderText="任务内容"/>
            <asp:BoundField DataField="Remarks" HeaderText="备注"/>
            <asp:ButtonField HeaderText="详情" ButtonType="Link" Text="详情" CommandName="detail"/>
            <asp:CommandField HeaderText="操作" ShowDeleteButton="true" ShowCancelButton="true"/>
        </Columns>
    </asp:GridView>

</asp:Content>
