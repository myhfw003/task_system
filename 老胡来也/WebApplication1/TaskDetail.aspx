﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskDetail.aspx.cs" Inherits="WebApplication1.TaskDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>
                        <label>Id</label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblId"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>任务名称：</label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblName"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>任务内容：</label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblContent"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>任务状态：</label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblStatus"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>创建者：</label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblUser"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>创建时间：</label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTime"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>备注：</label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblRemarks"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" ID="btnBack" Text="返回列表页" OnClick="btnBack_Click"/>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
