﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class TaskDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var see = Session["Username"];

            if (see == null)
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                var id = Request.QueryString["id"];

                if (!string.IsNullOrEmpty(id))
                {
                    var sql = string.Format(@"select a.Id,a.TaskName,a.TaskContent,a.TaskStatus,b.Username,a.Remarks from Task a
                                                 left join Users b on a.CreatedUserId = b.Id 
                                                 where a.Id={0}", id);

                    var dt = DbHelper.GetData(sql);

                    if (dt.Rows.Count > 0)
                    {
                        var row = dt.Rows[0];

                        this.lblId.Text = id;
                        this.lblName.Text = row["TaskName"].ToString();
                        this.lblContent.Text = row["TaskContent"].ToString();
                        this.lblStatus.Text = row["TaskStatus"].ToString();
                        this.lblUser.Text = row["Username"].ToString();
                        this.lblTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        this.lblRemarks.Text = row["Remarks"].ToString();
                    }
                }
            }
            
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}